ARG ARCH=
FROM archlinux:$ARCH

RUN pacman -Syu --needed --noconfirm base-devel git vim gnome-shell zsh wget

RUN echo "MAKEFLAGS=\"-j$(nproc)\"" >> /etc/makepkg.conf
RUN rm -rf /var/cache/pacman/pkg/*

COPY run.sh /usr/bin
RUN chmod +x /usr/bin/run.sh

# makepkg user and workdir
ARG user=makepkg
RUN useradd --system --create-home $user \
  && echo "$user ALL=(ALL:ALL) NOPASSWD:ALL" > /etc/sudoers.d/$user
RUN wget -O /etc/zsh/zshrc https://git.grml.org/f/grml-etc-core/etc/zsh/zshrc
USER $user
WORKDIR /home/$user

# Install yay
RUN git clone https://aur.archlinux.org/yay-bin.git \
  && cd yay-bin \
  && makepkg -sri --needed --noconfirm \
  && cd \
  # Clean up
  && rm -rf .cache yay

RUN if [ "$ARCH" = "aarch64" ]; \ 
	then sudo touch /boot/config.txt; \
	fi

ENTRYPOINT ["run.sh"]

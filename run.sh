#! /bin/bash

if test -z "$1";
then
	echo "ERROR: pls specify a software to build!"
	exit 1
fi

if test -z "$2";
then
	echo "ERROR: pls specify a git url to build!"
	exit 1
#	git_url="https://aur.archlinux.org"
#else
#	git_url=$2
fi

git clone $2 ~/source

cd ~/source

echo "INFO: installing all missing dependencies..."

makepkg --printsrcinfo > SINFO

sudo pacman -Syuq --noconfirm

while read -r -u 9 key value;
do
    if [ "$key" == "depends" ];
    then
        dep=$(echo "$value" | cut -d ' ' -f2 | cut -d '>' -f1)
        echo "installing $dep..."
        yay --noconfirm --noeditmenu --removemake -S "$dep"
    fi
done 9< "SINFO"

echo "found $(nproc) cores"
makepkg -s -c -C --noconfirm --noprogressbar | tee ~/$1-build.log
mkdir -p /home/makepkg/pkg
cp ./*.pkg.tar.* /home/makepkg/pkg


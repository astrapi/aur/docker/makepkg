#! /bin/bash

docker build -t makepkg:x86_64 --build-arg ARCH=x86_64 .
docker build -t makepkg:aarch64 --build-arg ARCH=aarch64 .

#docker build . -f Dockerfile --pull --no-cache -t makepkg
